	<!DOCTYPE html>
<html lang="en">
<head>
  <title>Shippable Assignment</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Importing jquery and bootstrap -->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <!-- Inline css -->
  <style>
  html, body {
        background-color: #fff0f5;
        height: 100%;
    }
    .fill-height {
        min-height: 100%;
        height:auto !important;
        height: 100%;
        padding-top: 200px;
    }
  </style>
</head>
<body>
    <div class="container fill-height">

      <form class="form-inline text-center" role="form" action="" method="POST">
          <div class="form1">
           <label>Enter Github repository Url for getting issues:</label><br/>
           <input type="text" id="url" name="url" style="width: 500px; padding: 2px;"><br/><br/>
           <button type="submit" class="btn btn-default" name="submit" style="background-color: black; color: white;">Submit</button>
          </div>    
      </form>
      <br/>
        <div style="margin-left: 200px;">
            <?php
        		if(isset($_POST['submit'])) {
            	    $var_url = $_POST['url'];

                  // Explode array to get the repos name and project name
            	    $var_url_array =  explode('/',$var_url);

            	    if(strcmp($var_url_array[0],"https:")||strcmp($var_url_array[1],"")||strcmp($var_url_array[2],"github.com")||empty($var_url_array[3])||empty($var_url_array[4])) {
            	        echo "<script language='javascript'>alert('Invalid Url');</script>";
                        die();
            	    }

                  // Github Issues
            	    $url = "https://api.github.com/repos/".$var_url_array[3]."/".$var_url_array[4];

            	    $output = getGithubIssues($url);
            	    $total_issues = $output["open_issues_count"];

                  echo "<b>URL: " . $var_url . "</b><br/><br/>";

                  // Total issues
            	    echo "<br>Total Issues: <b>".$total_issues."</b><br>"; 
            	    
            	    $time_24hr = date('Y-m-d\TH:i:s.Z\Z', strtotime('-1 day', time()));
            	    $url = "https://api.github.com/repos/".$var_url_array[3]."/".$var_url_array[4]."/issues?since=".$time_24hr;     
            	    $output = getGithubIssues($url);
            	    $issues_24hr = count($output);

                  // Issues opened in last 24 hrs
            	    echo "Issues opened in last 24 hours: <b>".$issues_24hr."</b><br>";

            	    $time_7days = date('Y-m-d\TH:i:s.Z\Z', strtotime('-7 day', time()));
            	    $url = "https://api.github.com/repos/".$var_url_array[3]."/".$var_url_array[4]."/issues?since=".$time_7days;
            	    $output = getGithubIssues($url);
            	    $issues_7days = count($output);

                  // Issues opened more than 24 hours ago but less than 7 days ago
            	    echo "Issues opened more than 24 hours ago but less than 7 days ago: <b>".($issues_last7days-$issues_24hr)."</b><br>";

                  // Issues opened more than 24 hours ago but less than 7 days ago
            	    echo "Issues opened more than 7 days ago: <b>".($total_open_issues-$issues_last7days)."</b><br>";
            	}

              // Function to get the response from API
              function getGithubIssues($url) {
                  $ch = curl_init();
                  curl_setopt($ch, CURLOPT_URL,$url);
                  curl_setopt($ch, CURLOPT_USERAGENT, "surabhi");
                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                  $output=curl_exec($ch);
                  curl_close($ch);
                  $output1=json_decode($output,true);
                  return $output1;
              }
            ?>
        </div>
    </div>
        
</body>
</html>

