Application URL:

http://52.89.62.117/

Technologies Used:

php, HTML

Solution:

I have used Github issues API(https://developer.github.com/v3/issues/).
I am also using 'since' parameters for getting the issues by date.

After getting the public url, I am breaking the API parts separated by '/' to get the last two parts
and make the url of format: https://api.github.com/repos/<repo-name>/<project-name>/issues?<params>

After getting the response from API, I am displaying the output.



Better Solution:

I could have this project by using AJAX calls without using PHP. 
This way UI wont be blocked and there will be no required for any server.
Also UI/UX could have been made much better.